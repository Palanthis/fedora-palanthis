#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

echo QT_QPA_PLATFORMTHEME=qt5ct > /etc/environment

sed -i 's/SELINUX=enforcing/SELINUX=disabled/' /etc/selinux/config

dnf remove -y transmission
