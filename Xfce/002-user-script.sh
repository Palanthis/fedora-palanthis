#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install Apps
sudo dnf install -y conky neofetch lolcat
sudo dnf install -y gcc kernel-devel kernel-headers dkms make bzip2 perl ccache

# Enable RPMFusion Repos
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Install Apps
sudo dnf install -y geany chromium tilix kvantum rhythmbox clementine VirtualBox smb4k megasync pulseeffects keepassxc
sudo dnf install -y lightdm-gtk-greeter-settings grub-customizer gvfs-smb vlc conky-manager megasync smb4k qbittorrent
sudo dnf install -y simplescreenrecorder flameshot kid3 audacity libreoffice qt5ct grsync youtube-dl unrar ffmpeg
sudo dnf install -y ffmpegthumbnailer cairo-dock cairo-dock-plug-ins gvfs-smb smb4k timeshift @virtualization virt-manager
sudo dnf install -y gnome-disk-utility

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/adobe-source-code-pro.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/buuf3.34.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/adapta.tar.gz -C /usr/share/themes/ --overwrite

# Tilix Themes
sudo [ -d ~/.config/tilix ] || sudo mkdir ~/.config/tilix
sudo tar xzf ../tarballs/tilix.tar.gz -C ~/.config/ --overwrite

# Install Conky
[ -d ~/.conky ] || mkdir ~/.conky
tar xzf ../tarballs/conky.tar.gz -C ~/.conky/

# Use all cores for make and compress
../use-all-cores.sh

# Add neofetch to .bashrc
echo 'neofetch | lolcat' >> ~/.bashrc

echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
