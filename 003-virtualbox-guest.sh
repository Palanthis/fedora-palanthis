#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo dnf update kernel*

KERN_DIR=/usr/src/kernels/`uname -r`/build

mkdir /media/VirtualBoxGuestAdditions

mount -r /dev/cdrom /media/VirtualBoxGuestAdditions

./media/VirtualBoxGuestAdditions/VBoxLinuxAdditions.run

Press Enter to reboot code
