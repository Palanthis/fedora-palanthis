#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install Apps
sudo dnf install -y conky neofetch lolcat kate dolphin-plugins openssl zstd zsh
sudo dnf install -y gcc kernel-devel kernel-headers dkms make bzip2 perl ccache
sudo dnf install -y @virtualization virt-manager fuse-encfs plasma-vault

# Enable RPMFusion Repos
sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Install Apps
#sudo dnf install -y kvantum rhythmbox keepassxc yt-dlp powerline powerline-fonts
#sudo dnf install -y gvfs-smb vlc smb4k ktorrent unrar krita p7zip
#sudo dnf install -y simplescreenrecorder flameshot kid3 audacity  grsync cairo-dock cairo-dock-plug-ins
#sudo dnf install -y ffmpegthumbs kdenlive yakuake p7zip-plugins

# Copy over some of my favorite fonts, themes and icons
sudo [ -d /usr/share/fonts/OTF ] || sudo mkdir /usr/share/fonts/OTF
sudo [ -d /usr/share/fonts/TTF ] || sudo mkdir /usr/share/fonts/TTF
sudo tar xzf ../tarballs/fonts-otf.tar.gz -C /usr/share/fonts/OTF/ --overwrite
sudo tar xzf ../tarballs/fonts-ttf.tar.gz -C /usr/share/fonts/TTF/ --overwrite
sudo tar xzf ../tarballs/adobe-source-code-pro.tar.gz -C /usr/share/fonts/OTF --overwrite
sudo tar xzf ../tarballs/buuf3.42.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/buuf-icons-for-plasma.tar.gz -C /usr/share/icons/ --overwrite
sudo tar xzf ../tarballs/aritim-dark.colorscheme.tar.gz -C ~/.local/share/konsole/ --overwrite
sudo tar xzf ../tarballs/aritim-dark-gtk.tar.gz -C /usr/share/themes/ --overwrite
sudo tar xzf ../tarballs/templates.tar.gz -C ~/.local/share/ --overwrite
sudo tar xzf ../tarballs/adapta.tar.gz -C /usr/share/themes/ --overwrite
sudo tar xzf ../tarballs/adapta-home.tar.gz -C ~/ --overwrite
sudo tar xzf ../tarballs/share.tar.gz -C /usr/share/ --overwrite


# Add neofetch to .bashrc
echo 'neofetch | lolcat' >> ~/.bashrc

# Use all cores for make and compress
../use-all-cores.sh

echo "All done! Press enter to reboot!"
read -n 1 -s -r -p "Press Enter to reboot or Ctrl+C to stay here."

sudo reboot
