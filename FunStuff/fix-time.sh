#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Enable network time
sudo systemctl enable systemd-networkd.service

sudo systemctl start systemd-networkd.service

sudo systemctl enable systemd-timesyncd.service

sudo systemctl start systemd-timesyncd.service

sudo timedatectl set-ntp true

sudo hwclock --systohc --utc

sudo timedatectl set-local-rtc 0

echo " "
echo "Let's make sure everything looks right!"

timedatectl status 
