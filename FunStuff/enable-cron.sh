#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Ensure Cron and Anacron are installed
sudo dnf install -y cronie cronie-anacron

# Enable Con service
sudo systemctl enable crond.service
