#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo dnf config-manager --add-repo=https://negativo17.org/repos/fedora-multimedia.repo

sudo dnf -y install HandBrake-gui HandBrake-cli

sudo dnf -y install makemkv

sudo dnf -y install libdvdcss

