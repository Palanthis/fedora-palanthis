#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

numberofcores=$(grep -c ^processor /proc/cpuinfo)


case $numberofcores in

    12)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j13 -l12"' >> ~/.bashrc
        ;;
    8)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j9 -l8"' >> ~/.bashrc
        ;;
    6)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j7 -l6"' >> ~/.bashrc
        ;;
    4)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j5 -l4"' >> ~/.bashrc
        ;;
    2)
        echo "You have " $numberofcores" cores."
        echo "Changing the compression settings for "$numberofcores" cores."
        echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
		echo 'export MAKEFLAGS="-j3 -l2"' >> ~/.bashrc
        ;;
    *)
        echo "We do not know how many cores you have."
        echo "Do it manually."
        ;;

esac

echo "################################################################"
echo "###  All cores will be used during building and compression ####"
echo "################################################################"
