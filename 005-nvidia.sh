#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Install Drivers
sudo dnf install -y akmod-nvidia
sudo dnf install -y xorg-x11-drv-nvidia-cuda
sudo dnf install -y vulkan
sudo dnf install -y xorg-x11-drv-nvidia-cuda-libs
sudo dnf install -y libva-vdpau-driver
sudo dnf install -y vdpauinfo
sudo dnf install -y libva-utils

# Copy over default config file
sudo cp /usr/share/X11/xorg.conf.d/nvidia.conf /etc/X11/xorg.conf.d/nvidia.conf
