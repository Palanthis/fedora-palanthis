#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis 
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

sudo dnf install -y libva-intel-driver libva-intel-hybrid-driver 
sudo dnf install -y libva-utils ffmpeg ffmpeg-libs
